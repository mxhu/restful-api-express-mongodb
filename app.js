const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const config = require('./config');
const mongoose = require('mongoose');

const Genre = require('./models/genre');
const Book = require('./models/book');

app.listen(3000);
console.log('Running on port 3000');
app.use(bodyParser.json()); // middleware

// Connect to Mongoose
mongoose.Promise  = require('bluebird');
mongoose.connect(config.dbConnection);
const db = mongoose.connection;
db.on('err', function(){
  console.log("there was an error connection to db")
});
db.once('open', function(){
  console.log("connected to db")
});

// router
app.get('/', function(req, res) {
  res.send('Please use /api/books or /api/genres');
});

// genre
app.get('/api/genres', function(req, res){ Genre.getGenres(req, res); });

app.post('/api/genres', function(req, res){ Genre.addGenre(req, res); });

app.put('/api/genres/:_id', function(req, res){ Genre.updateGenre(req, res); });

app.delete('/api/genres/:_id', function(req, res){ Genre.deleteGenre(req, res); });

// book
app.get('/api/books', function(req, res){ Book.getBooks(req, res); });

app.get('/api/books/:_id', function(req, res){ Book.getOneBook(req, res); });

app.post('/api/books', function(req, res){ Book.addBook(req, res); });

app.put('/api/books/:_id', function(req, res){ Book.updateBook(req, res); });

app.delete('/api/books/:_id', function(req, res){ Book.deleteBook(req, res); });