const mongoose = require('mongoose');

const bookSchema = mongoose.Schema({
  title:{
    type: String,
    required: true
  },
  genre:{
    type: String,
    required: true
  },
  description:{
    type: String
  },
  author:{
    type: String,
    required: true
  },
  publisher:{
    type: String
  },
  pages:{
    type: String
  },
  image_url:{
    type: String,
  },
  buy_url:{
    type: String
  },
  created_date:{
    type: Date,
    default: Date.now
  }
});

const Book = module.exports = mongoose.model('Book', bookSchema);

module.exports.getBooks = function(req, res) {
  Book.find().then(
    resp => {
      res.json(resp);
    }
  ).catch(
    err => {
      console.log(err);
    }
  );
}

module.exports.getOneBook = function(req, res) {
  Book.findById(req.params._id).then(
    resp => {
      res.json(resp);
    }
  ).catch(
    err => {
      console.log(err);
    }
  );
}

module.exports.addBook = function(req, res) {
  let newBook = req.body;
  Book.create(newBook).then(
    resp => {
      res.json(resp);
    }
  ).catch(
    err => {
      console.log(err);
    }
  );
}

module.exports.updateBook = function(req, res) {
  let query = {_id: req.params._id};
  let newBook = req.body;
  Book.findOneAndUpdate(query, newBook).then(
    resp => {
      res.json(resp);
    }
  ).catch(
    err => {
      console.log(err);
    }
  );
}

module.exports.deleteBook = function(req, res) {
  let query = {_id: req.params._id};
  Book.remove(query).then(
    resp => {
      res.json(resp);
    }
  ).catch(
    err => {
      console.log(err);
    }
  );
}