const mongoose = require('mongoose');

const genreSchema = mongoose.Schema({
  name:{
    type: String,
    required: true
  },
  created_date:{
    type: Date,
    default: Date.now
  }
});

const Genre = module.exports = mongoose.model('Genre', genreSchema);

module.exports.getGenres = function(req, res) {
  Genre.find().then(
    resp => {
      res.json(resp);
    }
  ).catch(
    err => {
      console.log(err);
    }
  );
}

module.exports.addGenre = function(req, res) {
  let newGenre = req.body; // put everything in the form in a genre object
  Genre.create(newGenre).then(
    resp => {
      res.json(resp);
    }
  ).catch(
    err => {
      console.log(err);
    }
  );
}

module.exports.updateGenre = function(req, res) {
  let query = {_id: req.params._id};
  let newGenre = req.body;
  Genre.findOneAndUpdate(query, newGenre).then(
    resp => {
      res.json(resp);
    }
  ).catch(
    err => {
      console.log(err);
    }
  );
}

module.exports.deleteGenre = function(req, res) {
  let query = {_id: req.params._id};
  Genre.remove(query).then(
    resp => {
      res.json(resp);
    }
  ).catch(
    err => {
      console.log(err);
    }
  );
}